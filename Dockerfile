# iperf3 running as server in a container

FROM debian:testing-slim

# install binary and remove cache
RUN apt update \
    && apt install -y iperf3 \
    && rm -rf /var/lib/apt/lists/*

# Expose the default iperf3 server port
EXPOSE 5201

CMD ["iperf3", "-s"]
